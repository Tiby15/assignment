package com.example.tiberiu.model;

/**
 * Created by Tiberiu on 12/14/2015.
 */
public class CrewMember {
    public String url;
    public String role;
    public String team;
    public String name;
    public CrewMember(String url,String role,String team,String name){
        this.url=url;
        this.role=role;
        this.team=team;
        this.name=name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTeam() {
        return team;
    }

    public void setTeam(String team) {
        this.team = team;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
