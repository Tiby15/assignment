package com.example.tiberiu.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;

/**
 * Created by Tiberiu on 12/13/2015.
 */
public class PersonIDs implements Parcelable {
    @Expose
    private Long trakt;
    @Expose
    private String slug;
    @Expose
    private String imdb;
    @Expose
    private Long tmdb;
    @Expose
    private Long tvrage;
    public Long getTrakt(){
        return trakt;
    }
    public String getSlug(){
        return slug;
    }
    public String getImdb(){
        return imdb;
    }
    public Long getTmdb(){
        return tmdb;
    }
    public Long getTvrage(){return tvrage;}

    protected PersonIDs(Parcel in) {
        trakt = in.readByte() == 0x00 ? null : in.readLong();
        slug = in.readString();
        imdb = in.readString();
        tmdb = in.readByte() == 0x00 ? null : in.readLong();
        tvrage = in.readByte() == 0x00 ? null : in.readLong();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (trakt == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeLong(trakt);
        }
        dest.writeString(slug);
        dest.writeString(imdb);
        if (tmdb == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeLong(tmdb);
        }
        if (tvrage == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeLong(tvrage);
        }
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<PersonIDs> CREATOR = new Parcelable.Creator<PersonIDs>() {
        @Override
        public PersonIDs createFromParcel(Parcel in) {
            return new PersonIDs(in);
        }

        @Override
        public PersonIDs[] newArray(int size) {
            return new PersonIDs[size];
        }
    };
}