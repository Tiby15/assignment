package com.example.tiberiu.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Tiberiu on 12/13/2015.
 */
public class PersonImages implements Parcelable {
    @Expose
    @SerializedName("headshot")
    private MultiImages headshot;
    @Expose
    @SerializedName("fanart")
    private MultiImages fanart;

    public MultiImages getHeadshots(){return headshot;}
    public MultiImages getFanart(){return fanart;}

    protected PersonImages(Parcel in) {
        headshot = (MultiImages) in.readValue(MultiImages.class.getClassLoader());
        fanart = (MultiImages) in.readValue(MultiImages.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(headshot);
        dest.writeValue(fanart);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<PersonImages> CREATOR = new Parcelable.Creator<PersonImages>() {
        @Override
        public PersonImages createFromParcel(Parcel in) {
            return new PersonImages(in);
        }

        @Override
        public PersonImages[] newArray(int size) {
            return new PersonImages[size];
        }
    };
}