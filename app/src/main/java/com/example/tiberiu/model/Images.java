package com.example.tiberiu.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;

/**
 * Created by Tiberiu on 12/13/2015.
 */
public class Images implements Parcelable {
    @Expose
    private MultiImages fanArt;
    @Expose
    private MultiImages poster;
    @Expose
    private SingleImage logo;
    @Expose
    private SingleImage clearArt;
    @Expose
    private SingleImage banner;
    @Expose
    private SingleImage thumb;

    public MultiImages getFanArt(){
        return fanArt;
    }
    public MultiImages getPoster(){
        return poster;
    }
    public SingleImage getLogo() {
        return logo;
    }
    public SingleImage getClearArt(){
        return clearArt;
    }
    public SingleImage getBanner() {
        return banner;
    }
    public SingleImage getThumb(){
        return thumb;
    }

    protected Images(Parcel in) {
        fanArt = (MultiImages) in.readValue(MultiImages.class.getClassLoader());
        poster = (MultiImages) in.readValue(MultiImages.class.getClassLoader());
        logo = (SingleImage) in.readValue(SingleImage.class.getClassLoader());
        clearArt = (SingleImage) in.readValue(SingleImage.class.getClassLoader());
        banner = (SingleImage) in.readValue(SingleImage.class.getClassLoader());
        thumb = (SingleImage) in.readValue(SingleImage.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(fanArt);
        dest.writeValue(poster);
        dest.writeValue(logo);
        dest.writeValue(clearArt);
        dest.writeValue(banner);
        dest.writeValue(thumb);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Images> CREATOR = new Parcelable.Creator<Images>() {
        @Override
        public Images createFromParcel(Parcel in) {
            return new Images(in);
        }

        @Override
        public Images[] newArray(int size) {
            return new Images[size];
        }
    };
}
