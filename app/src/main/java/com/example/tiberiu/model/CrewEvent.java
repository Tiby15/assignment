package com.example.tiberiu.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Tiberiu on 12/13/2015.
 */
public class CrewEvent implements Parcelable {

    @SerializedName("cast")
    @Expose
    private List<Cast> cast = new ArrayList<Cast>();
    @Expose
    public Crew crew;

    public List<Cast> getCast(){return cast;}
    public Crew getCrew(){return crew;}

    protected CrewEvent(Parcel in) {
        if (in.readByte() == 0x01) {
            cast = new ArrayList<Cast>();
            in.readList(cast, Cast.class.getClassLoader());
        } else {
            cast = null;
        }
        crew = (Crew) in.readValue(Crew.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (cast == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(cast);
        }
        dest.writeValue(crew);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<CrewEvent> CREATOR = new Parcelable.Creator<CrewEvent>() {
        @Override
        public CrewEvent createFromParcel(Parcel in) {
            return new CrewEvent(in);
        }

        @Override
        public CrewEvent[] newArray(int size) {
            return new CrewEvent[size];
        }
    };
}