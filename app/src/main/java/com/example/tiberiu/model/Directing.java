package com.example.tiberiu.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Tiberiu on 12/13/2015.
 */
public class Directing implements Parcelable {

    @SerializedName("job")
    @Expose
    private String job;
    @SerializedName("person")
    @Expose
    private Person person;

    /**
     *
     * @return
     * The job
     */
    public String getJob() {
        return job;
    }

    /**
     *
     * @param job
     * The job
     */
    public void setJob(String job) {
        this.job = job;
    }

    /**
     *
     * @return
     * The person
     */
    public Person getPerson() {
        return person;
    }

    /**
     *
     * @param person
     * The person
     */
    public void setPerson(Person person) {
        this.person = person;
    }


    protected Directing(Parcel in) {
        job = in.readString();
        person = (Person) in.readValue(Person.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(job);
        dest.writeValue(person);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Directing> CREATOR = new Parcelable.Creator<Directing>() {
        @Override
        public Directing createFromParcel(Parcel in) {
            return new Directing(in);
        }

        @Override
        public Directing[] newArray(int size) {
            return new Directing[size];
        }
    };
}