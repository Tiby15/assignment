package com.example.tiberiu.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tiberiu on 12/13/2015.
 */
public class Crew implements Parcelable {

    @SerializedName("directing")
    @Expose
    private List<Directing> directing = new ArrayList<Directing>();
    @SerializedName("writing")
    @Expose
    private List<Writing> writing = new ArrayList<Writing>();
    @SerializedName("production")
    @Expose
    private List<Production> production = new ArrayList<Production>();
    @SerializedName("editing")
    @Expose
    private List<Editing> editing = new ArrayList<Editing>();
    @SerializedName("sound")
    @Expose
    private List<Sound> sound = new ArrayList<Sound>();
    @SerializedName("art")
    @Expose
    private List<Art> art = new ArrayList<Art>();
    @SerializedName("crew")
    @Expose
    private List<Crew2> crew = new ArrayList<Crew2>();

    /**
     *
     * @return
     * The directing
     */
    public List<Directing> getDirecting() {
        return directing;
    }

    /**
     *
     * @param directing
     * The directing
     */
    public void setDirecting(List<Directing> directing) {
        this.directing = directing;
    }

    /**
     *
     * @return
     * The writing
     */
    public List<Writing> getWriting() {
        return writing;
    }

    /**
     *
     * @param writing
     * The writing
     */
    public void setWriting(List<Writing> writing) {
        this.writing = writing;
    }

    /**
     *
     * @return
     * The production
     */
    public List<Production> getProduction() {
        return production;
    }

    /**
     *
     * @param production
     * The production
     */
    public void setProduction(List<Production> production) {
        this.production = production;
    }

    /**
     *
     * @return
     * The editing
     */
    public List<Editing> getEditing() {
        return editing;
    }

    /**
     *
     * @param editing
     * The editing
     */
    public void setEditing(List<Editing> editing) {
        this.editing = editing;
    }

    /**
     *
     * @return
     * The sound
     */
    public List<Sound> getSound() {
        return sound;
    }

    /**
     *
     * @param sound
     * The sound
     */
    public void setSound(List<Sound> sound) {
        this.sound = sound;
    }

    /**
     *
     * @return
     * The art
     */
    public List<Art> getArt() {
        return art;
    }

    /**
     *
     * @param art
     * The art
     */
    public void setArt(List<Art> art) {
        this.art = art;
    }

    /**
     *
     * @return
     * The crew
     */
    public List<Crew2> getCrew() {
        return crew;
    }

    /**
     *
     * @param crew
     * The crew
     */
    public void setCrew(List<Crew2> crew) {
        this.crew = crew;
    }


    protected Crew(Parcel in) {
        if (in.readByte() == 0x01) {
            directing = new ArrayList<Directing>();
            in.readList(directing, Directing.class.getClassLoader());
        } else {
            directing = null;
        }
        if (in.readByte() == 0x01) {
            writing = new ArrayList<Writing>();
            in.readList(writing, Writing.class.getClassLoader());
        } else {
            writing = null;
        }
        if (in.readByte() == 0x01) {
            production = new ArrayList<Production>();
            in.readList(production, Production.class.getClassLoader());
        } else {
            production = null;
        }
        if (in.readByte() == 0x01) {
            editing = new ArrayList<Editing>();
            in.readList(editing, Editing.class.getClassLoader());
        } else {
            editing = null;
        }
        if (in.readByte() == 0x01) {
            sound = new ArrayList<Sound>();
            in.readList(sound, Sound.class.getClassLoader());
        } else {
            sound = null;
        }
        if (in.readByte() == 0x01) {
            art = new ArrayList<Art>();
            in.readList(art, Art.class.getClassLoader());
        } else {
            art = null;
        }
        if (in.readByte() == 0x01) {
            crew = new ArrayList<Crew2>();
            in.readList(crew, Crew2.class.getClassLoader());
        } else {
            crew = null;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (directing == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(directing);
        }
        if (writing == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(writing);
        }
        if (production == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(production);
        }
        if (editing == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(editing);
        }
        if (sound == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(sound);
        }
        if (art == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(art);
        }
        if (crew == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(crew);
        }
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Crew> CREATOR = new Parcelable.Creator<Crew>() {
        @Override
        public Crew createFromParcel(Parcel in) {
            return new Crew(in);
        }

        @Override
        public Crew[] newArray(int size) {
            return new Crew[size];
        }
    };
}