package com.example.tiberiu.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;

/**
 * Created by Tiberiu on 12/13/2015.
 */
public class SingleImage implements Parcelable {
    @Expose
    private String full;
    public String getFull(){
        return full;
    }

    protected SingleImage(Parcel in) {
        full = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(full);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<SingleImage> CREATOR = new Parcelable.Creator<SingleImage>() {
        @Override
        public SingleImage createFromParcel(Parcel in) {
            return new SingleImage(in);
        }

        @Override
        public SingleImage[] newArray(int size) {
            return new SingleImage[size];
        }
    };
}