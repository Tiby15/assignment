package com.example.tiberiu.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;

/**
 * Created by Tiberiu on 12/13/2015.
 */
public class Person implements Parcelable {
    @Expose
    private String name;
    @Expose
    private PersonIDs ids;
    @Expose
    private PersonImages images;

    public String getName(){return name;}
    public PersonIDs getIds(){return ids;}
    public PersonImages getPersonImages(){return images;}

    protected Person(Parcel in) {
        name = in.readString();
        ids = (PersonIDs) in.readValue(PersonIDs.class.getClassLoader());
        images = (PersonImages) in.readValue(PersonImages.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeValue(ids);
        dest.writeValue(images);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Person> CREATOR = new Parcelable.Creator<Person>() {
        @Override
        public Person createFromParcel(Parcel in) {
            return new Person(in);
        }

        @Override
        public Person[] newArray(int size) {
            return new Person[size];
        }
    };
}