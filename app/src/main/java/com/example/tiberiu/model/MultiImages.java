package com.example.tiberiu.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;

/**
 * Created by Tiberiu on 12/13/2015.
 */
public class MultiImages implements Parcelable {
    @Expose
    private String full;
    @Expose
    private String medium;
    @Expose
    private String thumb;

    public String getFull(){
        return full;
    }
    public String getMedium(){
        return medium;
    }
    public String getThumb(){
        return thumb;
    }

    protected MultiImages(Parcel in) {
        full = in.readString();
        medium = in.readString();
        thumb = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(full);
        dest.writeString(medium);
        dest.writeString(thumb);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<MultiImages> CREATOR = new Parcelable.Creator<MultiImages>() {
        @Override
        public MultiImages createFromParcel(Parcel in) {
            return new MultiImages(in);
        }

        @Override
        public MultiImages[] newArray(int size) {
            return new MultiImages[size];
        }
    };
}
