package com.example.tiberiu.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;

import java.util.ArrayList;

/**
 * Created by Tiberiu on 12/13/2015.
 */
public class Movie implements Parcelable {
    @Expose
    private String title;
    @Expose
    private Integer year;
    @Expose
    private Ids ids;
    @Expose
    private String overview;
    @Expose
    private Float rating;
    @Expose
    private ArrayList<String> genres;
    @Expose
    private Images images;

    public String getTitle(){
        return title;
    }
    public Integer getYear(){
        return year;
    }
    public Ids getIDs(){
        return ids;
    }
    public Images getImages(){return images;}
    public String getOverview(){return overview;}

    public Float getRating(){return rating;}

    public ArrayList<String> getGenres(){return genres;}

    protected Movie(Parcel in) {
        title = in.readString();
        year = in.readByte() == 0x00 ? null : in.readInt();
        ids = (Ids) in.readValue(Ids.class.getClassLoader());
        overview = in.readString();
        rating = in.readByte() == 0x00 ? null : in.readFloat();
        if (in.readByte() == 0x01) {
            genres = new ArrayList<String>();
            in.readList(genres, String.class.getClassLoader());
        } else {
            genres = null;
        }
        images = (Images) in.readValue(Images.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        if (year == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeInt(year);
        }
        dest.writeValue(ids);
        dest.writeString(overview);
        if (rating == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeFloat(rating);
        }
        if (genres == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(genres);
        }
        dest.writeValue(images);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Movie> CREATOR = new Parcelable.Creator<Movie>() {
        @Override
        public Movie createFromParcel(Parcel in) {
            return new Movie(in);
        }

        @Override
        public Movie[] newArray(int size) {
            return new Movie[size];
        }
    };
}
