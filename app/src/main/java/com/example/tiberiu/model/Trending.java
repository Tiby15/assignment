package com.example.tiberiu.model;

/**
 * Created by Tiberiu on 12/13/2015.
 */

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;

public class Trending implements Parcelable {


    @Expose
    private Integer watchers;
    @Expose
    private Movie movie;

    public Integer getWatchers() {
        return watchers;
    }

    public Movie getMovie() {
        return movie;
    }


    protected Trending(Parcel in) {
        watchers = in.readByte() == 0x00 ? null : in.readInt();
        movie = (Movie) in.readValue(Movie.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (watchers == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeInt(watchers);
        }
        dest.writeValue(movie);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Trending> CREATOR = new Parcelable.Creator<Trending>() {
        @Override
        public Trending createFromParcel(Parcel in) {
            return new Trending(in);
        }

        @Override
        public Trending[] newArray(int size) {
            return new Trending[size];
        }
    };
}