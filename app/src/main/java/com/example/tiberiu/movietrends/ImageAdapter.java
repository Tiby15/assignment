package com.example.tiberiu.movietrends;

/**
 * Created by Tiberiu on 12/11/2015.
 */
import java.net.URL;
import java.util.ArrayList;
import java.util.Random;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.tiberiu.utils.DynamicHeightImageView;
import com.example.tiberiu.utils.DynamicHeightTextView;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;


/***
 * ADAPTER
 */

public class ImageAdapter extends ArrayAdapter<String> {

    private static final String TAG = "ImageAdapter";


    static class ViewHolder {
        DynamicHeightTextView txtLineOne;
        DynamicHeightImageView imageView;
    }

    private final LayoutInflater mLayoutInflater;
    private final Random mRandom;
    public ArrayList<String> mBackgroundPictures;
    ImageLoader imageLoader;
    Bitmap backup;

    private static final SparseArray<Double> sPositionHeightRatios = new SparseArray<Double>();

    public ImageAdapter(final Context context, final int textViewResourceId) {
        super(context, textViewResourceId);
        mLayoutInflater = LayoutInflater.from(context);
        mRandom = new Random();
        mBackgroundPictures = new ArrayList<String>();
        imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(context));
        backup= BitmapFactory.decodeResource(context.getResources(),R.drawable.logo);
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {

        ViewHolder vh;
        if (convertView == null) {
            convertView = mLayoutInflater.inflate(R.layout.list_item_sample, parent, false);
            vh = new ViewHolder();
            vh.txtLineOne = (DynamicHeightTextView) convertView.findViewById(R.id.txt_line1);
            vh.imageView = (DynamicHeightImageView) convertView.findViewById(R.id.imageView);

            convertView.setTag(vh);
        }
        else {
            vh = (ViewHolder) convertView.getTag();
        }
        double positionHeight = getPositionRatio(position);
       /* int backgroundIndex = position >= mBackgroundColors.size() ?
                position % mBackgroundColors.size() : position;

        vh.imageView.setBackground(mBackgroundColors.get(backgroundIndex));
        */

        //Log.d(TAG, "getView position:" + position + " h:" + positionHeight);

        vh.txtLineOne.setHeightRatio(positionHeight*0.4);
        vh.imageView.setHeightRatio(positionHeight*0.6);
        vh.txtLineOne.setText(getItem(position));
        if(!mBackgroundPictures.get(position).equals(""))
            imageLoader.displayImage(mBackgroundPictures.get(position),vh.imageView);
        else
            vh.imageView.setImageBitmap(backup);
        return convertView;
    }

    private double getPositionRatio(final int position) {
        double ratio = sPositionHeightRatios.get(position, 0.0);
        // if not yet done generate and stash the columns height
        // in our real world scenario this will be determined by
        // some match based on the known height and width of the image
        // and maybe a helpful way to get the column height!
        if (ratio == 0) {
            ratio = getRandomHeightRatio();
            sPositionHeightRatios.append(position, ratio);
           // Log.d(TAG, "getPositionRatio:" + position + " ratio:" + ratio);
        }
        return ratio;
    }

    private double getRandomHeightRatio() {
        return (mRandom.nextDouble() / 2.0) + 1.0; // height will be 1.0 - 1.5 the width
    }
}