package com.example.tiberiu.movietrends;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tiberiu.error.ErrorEvent;
import com.example.tiberiu.model.Cast;
import com.example.tiberiu.model.Crew;
import com.example.tiberiu.model.CrewEvent;
import com.example.tiberiu.model.Movie;
import com.example.tiberiu.model.Trending;
import com.example.tiberiu.net.NetworkLayer;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import java.util.List;

import de.greenrobot.event.EventBus;


public class MovieActivity extends Activity {

    Movie mMovie;
    private ImageView movieImage;
    private TextView movietTitle;
    private TextView movieSummary;
    private TextView movieGenres;
    private TextView movieRatings;
    private ImageLoader imageLoader;
    private CrewEvent mCrew;
    private Button btnCrew;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie);
        getActionBar().hide();
        if (getIntent().getParcelableExtra("myMovie") == null)
            this.finish();
        else
            mMovie=getIntent().getParcelableExtra("myMovie");
        imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(this));
        NetworkLayer.getCrew(mMovie.getIDs().getSlug());
        setupUI();
    }

    public void setupUI(){
        movieImage=(ImageView)findViewById(R.id.movie_iv);
        if(mMovie.getImages().getPoster().getFull()!=null)
            imageLoader.displayImage(mMovie.getImages().getPoster().getFull(),movieImage);
        movietTitle=(TextView)findViewById(R.id.movie_title);
        movieSummary=(TextView)findViewById(R.id.description_summary);
        movieRatings=(TextView)findViewById(R.id.movie_ratings);
        movieGenres=(TextView)findViewById(R.id.movie_genres);
        movietTitle.setText(mMovie.getTitle());
        movieSummary.setText(mMovie.getOverview());
        movieRatings.setText("Ratings: ".concat(String.valueOf(mMovie.getRating())));
        String tempGenres="";
        for(int i=0;i<mMovie.getGenres().size();i++)
            tempGenres=tempGenres.concat(mMovie.getGenres().get(i)).concat(", ");
        movieGenres.setText("Genres :".concat(tempGenres.substring(0,tempGenres.length()-2)));
        btnCrew=(Button)findViewById(R.id.button_crew);
        btnCrew.setVisibility(View.GONE);
        btnCrew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentCrew=new Intent(MovieActivity.this,CrewActivity.class);
                intentCrew.putExtra("myCrew",mCrew);
                startActivity(intentCrew);
            }
        });
}
    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }
    public void onEvent(CrewEvent event) {
        mCrew=event;
        btnCrew.setVisibility(View.VISIBLE);
    }

    public void onEvent(ErrorEvent event) {
        if(event.getErrorStatus() == ErrorEvent.REQUEST_GET_CREW)
            Toast.makeText(this, "Could not get the crew for the movie", Toast.LENGTH_LONG).show();
    }
}
