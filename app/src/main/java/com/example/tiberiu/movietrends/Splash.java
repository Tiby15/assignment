package com.example.tiberiu.movietrends;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.widget.Toast;

import com.example.tiberiu.error.ErrorEvent;
import com.example.tiberiu.model.Trending;
import com.example.tiberiu.net.NetworkLayer;

import java.util.ArrayList;
import java.util.List;

import de.greenrobot.event.EventBus;


public class Splash extends Activity {
    public final int PAGE = 1;
    public final int LIMIT = 10;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        getActionBar().hide();

    }
    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(Splash.this);
        if(!isNetworkAvailable())
        {
            Toast.makeText(this,"This application needs an internet connection",Toast.LENGTH_LONG).show();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    finish();
                }
            }, 3500);

        }
        else
            NetworkLayer.getMovies(PAGE, LIMIT);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(Splash.this);
    }

    public void onEvent(ArrayList<Trending> event) {
        EventBus.getDefault().unregister(Splash.this);
        Intent mainIntent=new Intent(this,MainActivity.class);
        mainIntent.putParcelableArrayListExtra("movieList", event);
        startActivity(mainIntent);
        this.finish();
    }
    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
    public void onEvent(ErrorEvent event){
        if(event.getErrorStatus() == ErrorEvent.REQUEST_GET_TRENDING)
            Toast.makeText(this, "Could not get the trending movies", Toast.LENGTH_LONG).show();
    }
}

