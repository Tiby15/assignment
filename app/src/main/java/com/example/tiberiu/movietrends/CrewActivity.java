package com.example.tiberiu.movietrends;

import android.app.Activity;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.example.tiberiu.model.CrewEvent;
import com.example.tiberiu.model.CrewMember;

import java.util.ArrayList;

import se.emilsjolander.stickylistheaders.StickyListHeadersListView;


public class CrewActivity extends Activity {

    public CrewEvent mCrew;
    public ArrayList<CrewMember> crewMembers=new ArrayList<CrewMember>();
    private StickyListHeadersListView mList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crew);
        getActionBar().hide();
        if (getIntent().getParcelableExtra("myCrew") == null)
            this.finish();
        else
            mCrew=getIntent().getParcelableExtra("myCrew");
        composeListView();
        mList = (StickyListHeadersListView) findViewById(R.id.list_view);
        CrewAdapter mAdapter=new CrewAdapter(this,R.layout.list_item_sticky);
        mAdapter.updateCrew(crewMembers);
        mList.setAdapter(mAdapter);
        System.out.println(mAdapter.getCount());

    }

    public void composeListView() {
            for (int i = 0; i < mCrew.getCast().size(); i++) {
                if (mCrew.getCast().get(i).getPerson().getPersonImages() != null) {
                    if (mCrew.getCast().get(i).getPerson().getPersonImages().getHeadshots() != null)
                        if (mCrew.getCast().get(i).getPerson().getPersonImages().getHeadshots().getThumb() != null)
                            crewMembers.add(new CrewMember(mCrew.getCast().get(i).getPerson().getPersonImages().getHeadshots().getThumb(),
                                    mCrew.getCast().get(i).getCharacter(), "Cast", mCrew.getCast().get(i).getPerson().getName()));
                } else {
                    crewMembers.add(new CrewMember(null,
                            mCrew.getCast().get(i).getCharacter(), "Cast", mCrew.getCast().get(i).getPerson().getName()));
                }
            }
        if (mCrew.getCrew().getArt() != null)
            for (int i = 0; i < mCrew.getCrew().getArt().size(); i++) {
                crewMembers.add(new CrewMember(mCrew.getCrew().getArt().get(i).getPerson().getPersonImages().getHeadshots().getThumb(),
                        mCrew.getCrew().getArt().get(i).getJob(), "Art", mCrew.getCrew().getArt().get(i).getPerson().getName()));
            }
        if(mCrew.getCrew().getCrew()!=null)
            for (int i = 0; i < mCrew.getCrew().getCrew().size(); i++)
                crewMembers.add(new CrewMember(mCrew.getCrew().getCrew().get(i).getPerson().getPersonImages().getHeadshots().getThumb(),
                    mCrew.getCrew().getCrew().get(i).getJob(), "Crew", mCrew.getCrew().getCrew().get(i).getPerson().getName()));
        if(mCrew.getCrew().getSound()!=null) {
            for (int i = 0; i < mCrew.getCrew().getSound().size(); i++)
                crewMembers.add(new CrewMember(mCrew.getCrew().getSound().get(i).getPerson().getPersonImages().getHeadshots().getThumb(),
                        mCrew.getCrew().getSound().get(i).getJob(), "Sound", mCrew.getCrew().getSound().get(i).getPerson().getName()));
        }
        if(mCrew.getCrew().getDirecting()!=null) {
            for (int i = 0; i < mCrew.getCrew().getDirecting().size(); i++)
                crewMembers.add(new CrewMember(mCrew.getCrew().getDirecting().get(i).getPerson().getPersonImages().getHeadshots().getThumb(),
                        mCrew.getCrew().getDirecting().get(i).getJob(), "Directing", mCrew.getCrew().getDirecting().get(i).getPerson().getName()));
        }
        if(mCrew.getCrew().getEditing()!=null) {
            for (int i = 0; i < mCrew.getCrew().getEditing().size(); i++)
                crewMembers.add(new CrewMember(mCrew.getCrew().getEditing().get(i).getPerson().getPersonImages().getHeadshots().getThumb(),
                        mCrew.getCrew().getEditing().get(i).getJob(), "Editing", mCrew.getCrew().getEditing().get(i).getPerson().getName()));
        }
        if(mCrew.getCrew().getProduction()!=null) {
            for (int i = 0; i < mCrew.getCrew().getProduction().size(); i++)
                crewMembers.add(new CrewMember(mCrew.getCrew().getProduction().get(i).getPerson().getPersonImages().getHeadshots().getThumb(),
                        mCrew.getCrew().getProduction().get(i).getJob(), "Production", mCrew.getCrew().getProduction().get(i).getPerson().getName()));
        }
        if(mCrew.getCrew().getWriting()!=null) {
            for (int i = 0; i < mCrew.getCrew().getWriting().size(); i++)
                crewMembers.add(new CrewMember(mCrew.getCrew().getWriting().get(i).getPerson().getPersonImages().getHeadshots().getThumb(),
                        mCrew.getCrew().getWriting().get(i).getJob(), "Writing", mCrew.getCrew().getWriting().get(i).getPerson().getName()));
        }
    }
}
