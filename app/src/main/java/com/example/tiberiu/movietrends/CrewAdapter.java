package com.example.tiberiu.movietrends;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.tiberiu.model.CrewMember;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter;

/**
 * Created by Tiberiu on 12/14/2015.
 */
public class CrewAdapter extends ArrayAdapter<CrewMember> implements StickyListHeadersAdapter {

    private Context mContext;
    private List<CrewMember> mCrew;
    LayoutInflater mInflater;
    ImageLoader imageLoader;
    Bitmap backup;

    public CrewAdapter(Context context, int resource) {
        super(context, resource);
        mContext = context;
        mInflater = LayoutInflater.from(mContext);
        imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(context));
        mCrew = new LinkedList<CrewMember>();
        backup= BitmapFactory.decodeResource(context.getResources(), R.drawable.logo);
    }

    public void updateCrew(List<CrewMember> CrewMembers) {
        mCrew.addAll(CrewMembers);
        Collections.sort(mCrew, new Comparator<CrewMember>() {
            @Override
            public int compare(CrewMember lhs, CrewMember rhs) {
                return lhs.getTeam().compareTo(rhs.getTeam());
            }
        });
        this.notifyDataSetChanged();
    }
    public void clearCrew()
    {
        mCrew.clear();
        this.notifyDataSetChanged();
    }


    @Override
    public CrewMember getItem(int position) {
        return mCrew.get(position);
    }

    @Override
    public int getCount() {
        return mCrew.size();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            ViewHolder holder = new ViewHolder();

            convertView = mInflater.inflate(R.layout.list_item_sticky, parent, false);
            holder.mCardView = (RelativeLayout) convertView;
            convertView.setTag(holder);
        }

        ViewHolder holder = (ViewHolder) convertView.getTag();
       ((TextView) holder.mCardView.findViewById(R.id.crew_name)).setText(mCrew.get(position).getName());
       ((TextView) holder.mCardView.findViewById(R.id.crew_role)).setText(mCrew.get(position).getRole());
        if(mCrew.get(position).getUrl()!=null)
            imageLoader.displayImage(mCrew.get(position).getUrl(),((ImageView) holder.mCardView.findViewById(R.id.image_crew)));
        else
            ((ImageView) holder.mCardView.findViewById(R.id.image_crew)).setImageBitmap(backup);
        return convertView;
    }

    @Override
    public View getHeaderView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            HeaderViewHolder holder = new HeaderViewHolder();
            view = mInflater.inflate(R.layout.list_item_sticky_header, viewGroup, false);
            holder.brandName = (TextView) view.findViewById(R.id.sticky_header_team);
            view.setTag(holder);
        }

        HeaderViewHolder holder = (HeaderViewHolder) view.getTag();
        holder.brandName.setText(mCrew.get(i).getTeam());

        return view;
    }

    @Override
    public long getHeaderId(int i) {
        return mCrew.get(i).getTeam().hashCode();
    }



    public class ViewHolder {
        public RelativeLayout mCardView;
    }

    public class HeaderViewHolder {
        public TextView brandName;
    }
}