package com.example.tiberiu.movietrends;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.tiberiu.error.ErrorEvent;
import com.example.tiberiu.model.Trending;
import com.example.tiberiu.net.NetworkLayer;
import com.example.tiberiu.utils.StaggeredGridView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import de.greenrobot.event.EventBus;

/**
 * Created by Tiberiu on 12/13/2015.
 */
public class MainActivity extends Activity implements AbsListView.OnScrollListener, AbsListView.OnItemClickListener, AdapterView.OnItemLongClickListener {
    private static final String TAG = "MainActivity";
    private int page = 2;
    private static final int LIMIT = 10;
    private StaggeredGridView mGridView;
    private boolean mHasRequestedMore;
    private ImageAdapter mAdapter;
    private ArrayList<Trending> mMovies;
    private View footer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sgv);

        setTitle("Movie Trends");
        mGridView = (StaggeredGridView) findViewById(R.id.grid_view);
        LayoutInflater layoutInflater = getLayoutInflater();
        mAdapter = new ImageAdapter(this, R.id.txt_line1);
        mGridView.setAdapter(mAdapter);
        mGridView.setOnScrollListener(this);
        mGridView.setOnItemClickListener(this);
        mGridView.setOnItemLongClickListener(this);
        if (getIntent().getParcelableArrayListExtra("movieList") != null) {
            mMovies = getIntent().getParcelableArrayListExtra("movieList");
            addImages();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onScrollStateChanged(final AbsListView view, final int scrollState) {
        //   Log.d(TAG, "onScrollStateChanged:" + scrollState);
    }

    @Override
    public void onScroll(final AbsListView view, final int firstVisibleItem, final int visibleItemCount, final int totalItemCount) {
     /*   Log.d(TAG, "onScroll firstVisibleItem:" + firstVisibleItem +
                " visibleItemCount:" + visibleItemCount +
                " totalItemCount:" + totalItemCount);*/

        if (!mHasRequestedMore) {
            int lastInScreen = firstVisibleItem + visibleItemCount;
            if (lastInScreen >= totalItemCount) {
                // Log.d(TAG, "onScroll lastInScreen - so load more");
                mHasRequestedMore = true;
                onLoadMoreItems();
            }
        }
    }

    private void addImages() {
        for(int i=mAdapter.getCount();i< mMovies.size();i++){
            mAdapter.add(mMovies.get(i).getMovie().getTitle());
            if (mMovies.get(i).getMovie().getImages().getThumb().getFull() != null)
                mAdapter.mBackgroundPictures.add(mMovies.get(i).getMovie().getImages().getThumb().getFull());
            else
                mAdapter.mBackgroundPictures.add("");
        }
        mAdapter.notifyDataSetChanged();

    }

    private void onLoadMoreItems() {
        NetworkLayer.getMovies(page,LIMIT);
        page++;
        Toast.makeText(this, "Loading more movies", Toast.LENGTH_SHORT).show();
    }

    public void onEvent(List<Trending> event) {
        mMovies.addAll(event);
        addImages();
        mHasRequestedMore=false;
    }

    public void onEvent(ErrorEvent event) {
        if(event.getErrorStatus() == ErrorEvent.REQUEST_GET_TRENDING)
            Toast.makeText(this, "Could not get the trending movies", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
       // Toast.makeText(this, mMovies.get(position).getMovie().getTitle()+" has a rating of "+mMovies.get(position).getMovie().getOverview(), Toast.LENGTH_SHORT).show();
        Intent movieIntent=new Intent(MainActivity.this,MovieActivity.class);
        movieIntent.putExtra("myMovie",mMovies.get(position).getMovie());
        startActivity(movieIntent);
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
        Intent movieIntent=new Intent(MainActivity.this,MovieActivity.class);
        movieIntent.putExtra("myMovie",mMovies.get(position).getMovie());
        startActivity(movieIntent);
        return true;
    }

}

