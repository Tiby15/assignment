package com.example.tiberiu.net;

import android.webkit.WebView;

import com.example.tiberiu.App;
import com.example.tiberiu.error.ErrorEvent;
import com.example.tiberiu.model.CrewEvent;
import com.example.tiberiu.model.Trending;
import com.example.tiberiu.movietrends.R;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import de.greenrobot.event.EventBus;
import retrofit.Callback;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.Headers;
import retrofit.http.POST;
import retrofit.http.Path;
import retrofit.http.Query;



/**
 * Created by Tiberiu on 12/12/2015.
 */
public class NetworkLayer {

    private static final String API_ENDPOINT = "https://api-v2launch.trakt.tv";
    private static final String CLIENT_ID = "0e7e55d561c7e688868a5ea7d2c82b17e59fde95fbc2437e809b1449850d4162";
    private static final String API_VERSION = "2";
    private static final String EXTENDED_FULL = "full,images";
    private static final String EXTENDED_IMAGES = "images";

    private static TraktService getTraktService() {
        RequestInterceptor requestInterceptor = new RequestInterceptor() {
            @Override
            public void intercept(RequestFacade request) {
                request.addHeader("trakt-api-version", API_VERSION);
                request.addHeader("trakt-api-key", CLIENT_ID);
            }
        };
        RestAdapter restAdapter = new RestAdapter.Builder().setLogLevel(RestAdapter.LogLevel.FULL).setRequestInterceptor(requestInterceptor).setEndpoint(API_ENDPOINT).build();
        return restAdapter.create(TraktService.class);
    }

    public static void getMovies(int page, int limit) {
        Callback<ArrayList<Trending>> callback = new Callback<ArrayList<Trending>>() {
            @Override
            public void failure(RetrofitError err) {
                System.out.println(err.getMessage());
                handleErrorEvent(err, ErrorEvent.REQUEST_GET_TRENDING);
            }

            @Override
            public void success(ArrayList<Trending> container, Response r) {
                System.out.println("Posting the event");
                EventBus.getDefault().post(container);
            }
        };
        NetworkLayer.getTraktService().getMovies(page, limit, EXTENDED_FULL, callback);
    }
    public static void getCrew(String id) {
        Callback<CrewEvent> callback = new Callback<CrewEvent>() {
            @Override
            public void failure(RetrofitError err) {
                System.out.println(err.getMessage());
                System.out.println("GOT A BIG RETROFIT ERROR");
                handleErrorEvent(err, ErrorEvent.REQUEST_GET_CREW);
            }

            @Override
            public void success(CrewEvent container, Response r) {
                EventBus.getDefault().post(container);
            }
        };
        NetworkLayer.getTraktService().getCrew(id, EXTENDED_IMAGES, callback);
    }

    private static void handleErrorEvent(RetrofitError err, int requestId) {
        ErrorEvent eEvent = new ErrorEvent();
        eEvent.setRequestID(requestId);
        eEvent.setRequestURL(err.getUrl());
        Response r = err.getResponse();
        if (r != null) {
            eEvent.setErrorStatus(r.getStatus());
            eEvent.setErrorReason(r.getReason());
            if (r.getBody() != null) {
                BufferedReader br;
                try {
                    br = new BufferedReader(new InputStreamReader(r.getBody().in()));
                    StringBuilder total = new StringBuilder();
                    String errorBody;
                    while ((errorBody = br.readLine()) != null) {
                        total.append(errorBody);
                    }
                    eEvent.setErrorBody(total.toString());
                } catch (IOException e) {
                    eEvent.setErrorBody("");
                }
            }
        } else {
            if (err.isNetworkError()) {
                eEvent.setErrorStatus(601);
            } else {
                eEvent.setErrorStatus(600);
            }
        }
        EventBus.getDefault().post(eEvent);
    }


    private interface TraktService {

        @GET("/movies/trending")
        public void getMovies(
                @Query("page") Integer page,
                @Query("limit") Integer limit,
                @Query("extended") String query,
                Callback<ArrayList<Trending>> callback);

        @GET("/movies/{id}/people")
        public void getCrew(
                @Path("id") String id,
                @Query("extended") String query,
                Callback<CrewEvent> callback);
    }
}
