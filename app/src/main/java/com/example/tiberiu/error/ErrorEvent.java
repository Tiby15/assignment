package com.example.tiberiu.error;

/**
 * Created by Tiberiu on 12/12/2015.
 */
public class ErrorEvent {

    /* Requests Ids from asynchronous network calls */
    public static final int REQUEST_GET_TRENDING = 1;
    public static final int REQUEST_GET_CREW=2;
    public static final int ERROR_BAD_REQUEST = 400;
    public static final int ERROR_AUTH = 401;
    public static final int ERROR_FORBIDDEN = 403;
    public static final int ERROR_NOT_FOUND = 404;

    //5xx Client Error
    public static final int ERROR_INTERNAL_SERVER_ERROR = 500;

    /* Custom error codes */
    //6xx Custom Generic Error
    public static final int ERROR_GENERIC = 600;
    public static final int ERROR_NETWORK = 601;

    /*Attributes*/
    private int requestID;//The request identifying the call
    private String requestURL;//The request URL
    private int errorStatus;//The type of error
    private String errorReason;//Retrofit's error reason
    private String errorBody;//Retrofit's error body

    public int getRequestID() {
        return requestID;
    }

    public void setRequestID(int requestID) {
        this.requestID = requestID;
    }

    public String getRequestURL() {
        return requestURL;
    }

    public void setRequestURL(String requestURL) {
        this.requestURL = requestURL;
    }

    public int getErrorStatus() {
        return errorStatus;
    }

    public void setErrorStatus(int errorStatus) {
        this.errorStatus = errorStatus;
    }

    public String getErrorReason() {
        return errorReason;
    }

    public void setErrorReason(String errorReason) {
        this.errorReason = errorReason;
    }

    public String getErrorBody() {
        return errorBody;
    }

    public void setErrorBody(String errorBody) {
        this.errorBody = errorBody;
    }

    /* Possible API error codes */

}

