package com.example.tiberiu;

import android.app.Application;
import android.content.Context;

import com.example.tiberiu.net.NetworkLayer;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.utils.StorageUtils;
import com.squareup.okhttp.Response;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;

/**
 * Created by Tiberiu on 12/12/2015.
 */
public class App extends Application {

    private static Context mContext;
    private static Context mBaseContext;
    private static Context mApplication;
    public static Context getContext() {
        return mContext;
    }

    public static Context getBaseContextFromApp() {
        return mBaseContext;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = this;
        mBaseContext = this.getBaseContext();
        mApplication = this;
        File cacheDir = StorageUtils.getCacheDirectory(getContext());
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getContext()).build();
        DisplayImageOptions options = new DisplayImageOptions.Builder().cacheOnDisk(true).delayBeforeLoading(1000).build();
    }
}